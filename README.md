# 666-1977

`2 books` `all files encrypted`

---

[Functions of Several Variables](./bok%253A978-1-4684-9461-7.zip)
<br>
Wendell Fleming in Undergraduate Texts in Mathematics (1977)

---

[A First Course in Real Analysis](./bok%253A978-1-4615-9990-6.zip)
<br>
M. H. Protter, C. B. Morrey Jr. in Undergraduate Texts in Mathematics (1977)

---
